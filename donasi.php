<?php 
  error_reporting(E_ALL^(E_NOTICE|E_WARNING));
  include"config/config.php";
?>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>Donasi - PIDONA</title>
  <meta content="" name="descriptison">
  <meta content="" name="keywords">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Jost:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">

  <!-- Vendor CSS Files -->
  <link href="assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="assets/vendor/icofont/icofont.min.css" rel="stylesheet">
  <link href="assets/vendor/boxicons/css/boxicons.min.css" rel="stylesheet">
  <link href="assets/vendor/remixicon/remixicon.css" rel="stylesheet">
  <link href="assets/vendor/venobox/venobox.css" rel="stylesheet">
  <link href="assets/vendor/owl.carousel/assets/owl.carousel.min.css" rel="stylesheet">
  <link href="assets/vendor/aos/aos.css" rel="stylesheet">

  <!-- Template Main CSS File -->
  <link href="assets/css/style.css" rel="stylesheet">
</head>

<body>

  <!-- ======= Header ======= -->
  <header id="header" class="fixed-top header-inner-pages">
    <div class="container d-flex align-items-center">

      <h1 class="logo mr-auto"><a href="index.html">PIDONA</a></h1>
      <!-- Bisi ek make logo -->
      <!-- <a href="index.php" class="logo mr-auto"><img src="assets/img/support/doni.png" alt="" class="img-fluid"></a>-->

      <nav class="nav-menu d-none d-lg-block">
        <ul>
          <li class="active"><a href="index.php">Home</a></li>
          <li><a href="data.php">Data</a></li>        
          <li><a href="berita.php">Berita</a></li>
        </ul>
      </nav><!-- .nav-menu -->

    </div>
  </header><!-- End Header -->

  <main id="main">

    <!-- ======= Breadcrumbs ======= -->
    <section id="breadcrumbs" class="breadcrumbs">
      <div class="container">

        <ol>
          <li><a href="index.php">Home</a></li>
          <li>Donasi</li>
        </ol>
        <h2>Halaman Donasi</h2>

      </div>
    </section><!-- End Breadcrumbs -->

    <!-- ======= Donasi Section ======= -->
    <section id="portfolio-details" class="portfolio-details">
      <div class="container">

        <div class="portfolio-description">
          <?php  
            $count = mysqli_num_rows(mysqli_query($con, "SELECT * FROM donasi"));
            $uang = $con->query("select * from donasi where status='1' ");
              while($donasi = $uang->fetch_assoc()):

                number_format($donasi['jumlah']);
                $total = $total+$donasi['jumlah'];

              endwhile;
            $jum = ($total/1000000000)*100;
          ?>
          <div class="row text-center">
                    
                    <div class="col-md-3 mt-2">
                      <div class="card"
                        style="border-top: 2px solid #37517e;
                        transition: all ease-in-out 0.3s;
                        box-shadow: 0px 0 25px 0 rgba(0, 0, 0, 0.1);">
                        <h6 class="text-center mt-3">DATA DONATUR</h6>
                        <h3 style="color: #37517e;" class="text-center">
                          <?= $count; ?>
                        </h3>
                      </div>
                    </div>

                    <div class="col-md-6 mt-2">
                      <div class="card"
                        style="border-top: 2px solid #37517e;
                        transition: all ease-in-out 0.3s;
                        box-shadow: 0px 0 25px 0 rgba(0, 0, 0, 0.1);">
                        <h4 class="text-center mt-3">TARGET</h4>
                        <hr>
                        <h3 style="color: #37517e;text-align: center;">
                          <?php echo number_format($total); ?>.- / 500,000,000.-
                        </h3>
                        <div class="isi mb-4 mt-0 mr-4 ml-4">
                          <div class="progress" style="border-radius: 30px;">
                            <div class="progress-bar bg-warning" role="progressbar" style="width: <?= $jum; ?>%"  aria-valuenow="10" aria-valuemin="0" aria-valuemax="100">
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>

                    <div class="col-md-3 mt-2">
                      <div class="card"
                        style="border-top: 2px solid #37517e;
                        transition: all ease-in-out 0.3s;
                        box-shadow: 0px 0 25px 0 rgba(0, 0, 0, 0.1);">
                        <h6 class="text-center mt-3">BERAKHIR PADA</h6>
                        <h3 style="color: #37517e;text-align: center;" data-toggle="counter-up">
                          12 - 13 - 2023
                        </h3>
                      </div>
                    </div>
                  </div>

                  <div class="row mt-5">

                    <div class="col-md-6 mt-3">
                      <div class="card"
                        style="border-top: 2px solid #37517e;
                        transition: all ease-in-out 0.3s;
                        box-shadow: 0px 0 25px 0 rgba(0, 0, 0, 0.1);">
                        <div class="card-header">
                          <div>
                            <img src="assets/img/donasi.png" alt="gambar" width="100%">
                          </div>
                        </div>
                        <div class="card-body">
                          <h6><b>Group    : COVID-19</b></h6>
                          <h6><b>Kategori : Bencana</b></h6>
                          <h6><b>Daerah   : Seluruh Indonesia</b></h6>
                        </div>
                      </div>
                    </div>

                    <div class="col-md-6 mt-3">
                      <div class="card"
                        style="border-top: 2px solid #37517e;
                        transition: all ease-in-out 0.3s;
                        box-shadow: 0px 0 25px 0 rgba(0, 0, 0, 0.1);">
                        <form role="form" class="mt-4 mb-4 ml-4 mr-4" method='post' enctype="multipart/form-data" action='kirimdonasi.php'>
                          <div class="row mt-3">
                            <div class="col-md-6">
                              <div class="form-group">
                                <label>Jumlah Donasi</label>
                                  <input type="text" class="form-control" id="jumlah" name="jumlah" style="border-radius: 25px;" required="">
                              </div>
                            </div>
                            <div class="col-md-6">
                              <div class="form-group">
                                <label>No Handphone</label>
                                  <input type="text" class="form-control" id="nohp" name="nohp" style="border-radius: 25px;" required="">
                              </div>
                            </div>
                          </div>
                          <div class="row mt-3">
                            <div class="col-md-6">
                            <div class="form-group">
                                <label>Nama Lengkap</label>
                                  <input type="text" class="form-control" id="nama" name="nama" style="border-radius: 25px;" required="">
                              </div>
                            </div>
                            <div class="col-md-6">
                              <div class="form-group">
                                <label>Email</label>
                                  <input type="text" class="form-control" id="email" name="email" style="border-radius: 25px;" required="">
                              </div>
                            </div>
                          </div>
                          <div class="form-group">
                            <label>Catatan</label>
                            <textarea class="form-control" id="catatan" name="catatan" style="border-radius: 25px;" required="">
                            </textarea>
                          </div>
                          <div class="form-group">
                            <label for="bukti">Bukti Transfer</label>
                            <input type="file" class="form-control-file" name="fupload" id="bukti" required="">
                          </div>
                          <div class="text-right">
                            <button type="submit" name="kirim" class="btn btn-primary"
                            style="background-color: #37517e;border-radius: 25px;">
                              Kirim Donasi
                            </button>
                          </div>

                        </form>
                      </div>
                    </div>
                  </div>
        </div>
      </div>
    </section><!-- End Donasi Section -->

  </main><!-- End #main -->

  <!-- ======= Footer ======= -->
  <footer id="footer">

    <!-- ======= support Section ======= -->
    <section id="cliens" class="cliens section-bg">
      <div class="container">

        <div class="row" data-aos="zoom-in">

          <div class="col-lg-3 col-md-4 col-6 d-flex align-items-center justify-content-center">
            <img src="assets/img/support/doni.png" class="img-fluid" alt="">
          </div>

          <div class="col-lg-3 col-md-4 col-6 d-flex align-items-center justify-content-center">
            <img src="assets/img/support/doni.png" class="img-fluid" alt="">
          </div>

          <div class="col-lg-3 col-md-4 col-6 d-flex align-items-center justify-content-center">
            <img src="assets/img/support/doni.png" class="img-fluid" alt="">
          </div>

          <div class="col-lg-3 col-md-4 col-6 d-flex align-items-center justify-content-center">
            <img src="assets/img/support/doni.png" class="img-fluid" alt="">
          </div>

        </div>

      </div>
    </section><!-- End support Section -->

    <div class="container footer-bottom clearfix">
      <div class="copyright">
        &copy; Copyright <strong><span>PIDONA</span></strong>. All Rights Reserved
      </div>
      <div class="credits">
        Develop by Doni Anggara
      </div>
    </div>
  </footer><!-- End Footer -->

  <a href="#" class="back-to-top"><i class="ri-arrow-up-line"></i></a>
  <div id="preloader"></div>

  <!-- Vendor JS Files -->
  <script src="assets/vendor/jquery/jquery.min.js"></script>
  <script src="assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="assets/vendor/jquery.easing/jquery.easing.min.js"></script>
  <script src="assets/vendor/php-email-form/validate.js"></script>
  <script src="assets/vendor/waypoints/jquery.waypoints.min.js"></script>
  <script src="assets/vendor/isotope-layout/isotope.pkgd.min.js"></script>
  <script src="assets/vendor/venobox/venobox.min.js"></script>
  <script src="assets/vendor/owl.carousel/owl.carousel.min.js"></script>
  <script src="assets/vendor/aos/aos.js"></script>

  <!-- Template Main JS File -->
  <script src="assets/js/main.js"></script>

</body>

</html>