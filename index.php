<?php 
  include"config/config.php";
?>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>PIDONA</title>
  <meta content="" name="descriptison">
  <meta content="" name="keywords">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Jost:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">

  <!-- Vendor CSS Files -->
  <link href="assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="assets/vendor/icofont/icofont.min.css" rel="stylesheet">
  <link href="assets/vendor/boxicons/css/boxicons.min.css" rel="stylesheet">
  <link href="assets/vendor/remixicon/remixicon.css" rel="stylesheet">
  <link href="assets/vendor/venobox/venobox.css" rel="stylesheet">
  <link href="assets/vendor/owl.carousel/assets/owl.carousel.min.css" rel="stylesheet">
  <link href="assets/vendor/aos/aos.css" rel="stylesheet">

  <!-- Template Main CSS File -->
  <link href="assets/css/style.css" rel="stylesheet">
</head>

<body>

  <!-- ======= Header ======= -->
  <header id="header" class="fixed-top ">
    <div class="container d-flex align-items-center">

      <h1 class="logo mr-auto"><a href="index.html">PIDONA</a></h1>
      <!-- Bisi ek make logo -->
      <!-- <a href="index.php" class="logo mr-auto"><img src="assets/img/support/doni.png" alt="" class="img-fluid"></a>-->

      <nav class="nav-menu d-none d-lg-block">
        <ul>
          <li class="active"><a href="index.php">Home</a></li>
          <li><a href="#berita">Berita</a></li>        
          <li><a href="#data">Data</a></li>
          <li><a href="#edukasi">Edukasi</a></li>
          <li><a href="#donasi">Donasi</a></li>
          <li><a href="#faq">FAQ</a></li>
          <li><a href="#contact">Contact</a></li>
        </ul>
      </nav><!-- .nav-menu -->

    </div>
  </header><!-- End Header -->

  <!-- ======= Hero Section ======= -->
  <section id="hero" class="d-flex align-items-center">

    <div class="container pt-3 pb-3">
      <div class="row">
        <div class="col-lg-6 d-flex flex-column justify-content-center pt-4 pt-lg-0 order-2 order-lg-1" data-aos="fade-up" data-aos-delay="200">
          <h1>Penyedia informasi dan donasi corona</h1>
          <h2>Tetap jaga kesehatan, mari lawan virus corona !</h2>
          <div class="d-lg-flex">
            <a href="#data" class="btn-get-started scrollto">Lihat Data</a>
          </div>
        </div>
        <div class="col-lg-6 order-1 order-lg-2 hero-img" data-aos="zoom-in" data-aos-delay="200">
          <img src="assets/img/header.png" class="img-fluid animated" alt="">
        </div>
      </div>
    </div>

  </section><!-- End Hero -->

  <main id="main">    

    <!-- ======= Berita Section ======= -->
    <section id="berita" class="team">
      <div class="container" data-aos="fade-up">

        <div class="section-title">
          <h2>Berita Terbaru</h2>
          <p>Lihat Berita seputar virus corona disini !</p>
        </div>

        <div class="row">
          <?php 
            $query =  $con->query("select * from berita order by id desc LIMIT 4");
            while($berita = $query->fetch_assoc()):
          ?>
          <div class="col-lg-6">
            <div class="member d-flex align-items-start" data-aos="zoom-in" data-aos-delay="100">
              <div class="col-md-6 pi">
                <img src="assets/img/berita/<?php echo $berita['nama_file'];?>" class="img-fluid" alt="gambar">
                </div><br>  
              <div class="member-info">
                <h4><?= $berita['judul']; ?></h4>
                <span><i><?= date('d F Y', strtotime($berita['tanggal'])); ?></i></span>
                <p>
                  <?php 
                    $deskpendek = $berita['deskripsi'];
                      if (strlen($berita['deskripsi']) > 30) $berita['deskripsi'] = substr($berita['deskripsi'], 0, 30) . "........"; 
                        echo $berita['deskripsi'];
                          // echo strlen($deskpendek);
                  ?>
                </p>
                <div class="pt-4">
                  <a href="detailberita.php?id=<?= $berita['id']; ?>" class="btn btn-primary" style="background-color: #47b2e4;text-align: right;">Lihat</a>
                </div>
              </div>
            </div>
          </div>
          <?php endwhile; ?>
        </div>
        <br>
        <div class="row text-center">
          <div class="col-md-12 text-center">
            <div class="text-center">
              <a href="berita.php"><p class="btn btn-primary" style="background-color: #47b2e4;border-radius: 25px;">Lihat Semua Berita</p></a>
            </div>
          </div>
        </div>
      </div>
    </section><!-- End Team Section -->

    <!-- ======= Data Section ======= -->
    <section id="data" class="services section-bg">
      <div class="container" data-aos="fade-up">

        <div class="section-title">
          <h2>Data Corona</h2>
          <p>Lihat persebaran virus corona di Indonesia !</p>
        </div>
        <?php  
          $data = file_get_contents('https://api.kawalcorona.com/indonesia/');
          $indonesia = json_decode($data, true);
        ?>
        <div class="row text-center">
            <?php    
              foreach ($indonesia as $idn) :
            ?>
            <div class="col-md-4 mt-2">
              <div class="card"
                style="border-top: 2px solid #37517e;
                transition: all ease-in-out 0.3s;
                box-shadow: 0px 0 25px 0 rgba(0, 0, 0, 0.1);">
                <h6 class="text-center mt-3">DATA POSITIF</h6>
                <h3 style="color: #37517e;" class="text-center">
                  <?= $idn['positif']; ?>
                </h3>
              </div>
            </div>

            <div class="col-md-4 mt-2">
              <div class="card"
                style="border-top: 2px solid #37517e;
                transition: all ease-in-out 0.3s;
                box-shadow: 0px 0 25px 0 rgba(0, 0, 0, 0.1);">
                <h6 class="text-center mt-3">DATA SEMBUH</h6>
                <h3 style="color: #37517e;" class="text-center">
                  <?= $idn['sembuh']; ?>
                </h3>
              </div>
            </div>

            <div class="col-md-4 mt-2">
              <div class="card"
                style="border-top: 2px solid #37517e;
                transition: all ease-in-out 0.3s;
                box-shadow: 0px 0 25px 0 rgba(0, 0, 0, 0.1);">
                <h6 class="text-center mt-3">DATA MENINGGAL</h6>
                <h3 style="color: #37517e;text-align: center;" data-toggle="counter-up">
                  <?= $idn['meninggal']; ?>
                </h3>
              </div>
            </div>
            <?php    
              endforeach;
            ?>
        </div>
        <br>
        <div class="row text-center">
          <div class="col-md-12 text-center">
            <div class="text-center">
              <a href="data.php"><p class="btn btn-primary" style="background-color: #47b2e4;border-radius: 25px;">Lihat Detail</p></a>
            </div>
          </div>
        </div>
      </div>
    </section><!-- End Data Section -->

    <!-- ======= Edukasi Section ======= -->
    <section id="edukasi" class="about">
      <div class="container text-center" data-aos="fade-up">

        <div class="section-title">
          <h2>Lindungi Diri Kita</h2>
        </div>

        <div class="row content text-center">
          <div class="col-xl-3 col-md-6 d-flex align-items-stretch pt-3" data-aos="zoom-in" data-aos-delay="100">
            <div class="card" style="background-color: #fff;
              box-shadow: 0px 0 25px 0 rgba(0, 0, 0, 0.1);
              border-top:2px solid #37517e; border-radius: 20px;">
              <div class="icon text-center pl-1 pr-1 pt-2 pb-1"><img src="assets/img/cuci.png" class="img-around"></div>
              <h4 class="text-center pl-1 pr-1 pb-2"><a href="">Cuci tangan secara berkala menggunakan sabun</a></h4>
            </div>
          </div>

          <div class="col-xl-3 col-md-6 d-flex align-items-stretch pt-3" data-aos="zoom-in" data-aos-delay="100">
            <div class="card" style="background-color: #fff;
              box-shadow: 0px 0 25px 0 rgba(0, 0, 0, 0.1);
              border-top:2px solid #37517e; border-radius: 20px;">
              <div class="icon text-center pl-1 pr-1 pt-2 pb-1"><img src="assets/img/sd.png" class="img-around"></div>
              <h4 class="text-center pl-1 pr-1 pb-2"><a href="">Lakukan Social Distancing untuk menghentikan penyebaran virus corona</a></h4>
            </div>
          </div>

          <div class="col-xl-3 col-md-6 d-flex align-items-stretch pt-3" data-aos="zoom-in" data-aos-delay="100">
            <div class="card" style="background-color: #fff;
              box-shadow: 0px 0 25px 0 rgba(0, 0, 0, 0.1);
              border-top:2px solid #37517e; border-radius: 20px;">
              <div class="icon text-center pl-1 pr-1 pt-2 pb-1"><img src="assets/img/muka.png" class="img-around"></div>
              <h4 class="text-center pl-1 pr-1 pb-2"><a href="">Usahakan Jangan sentuh muka, karena tangan dapat membawa virus</a></h4>
            </div>
          </div>

          <div class="col-xl-3 col-md-6 d-flex align-items-stretch pt-3" data-aos="zoom-in" data-aos-delay="100">
            <div class="card" style="background-color: #fff;
              box-shadow: 0px 0 25px 0 rgba(0, 0, 0, 0.1);
              border-top:2px solid #37517e; border-radius: 20px;">
              <div class="icon text-center pl-1 pr-1 pt-2 pb-1"><img src="assets/img/hm.png" class="img-around"></div>
              <h4 class="text-center pl-1 pr-1 pb-2"><a href="">Tutup hidung dan mulut ketika bersin dan batuk</a></h4>
            </div>
          </div>
        </div>
      </div>
    </section>

    <section id="why-us" class="why-us section-bg">
      <div class="container-fluid" data-aos="fade-up">
        <div class="row">
          <div class="col-lg-7 d-flex flex-column justify-content-center align-items-stretch  order-2 order-lg-1">
            <div class="content mb-3">
              <h3><strong>Lindungi orang-orang sekeliling kita</strong></h3>
            </div>
            <div class="accordion-list">
              <ul>
                <li>
                  <a data-toggle="collapse" class="collapse" href="#accordion-list-1"><span>01</span> Diam dirumah ketika sedang sakit <i class="bx bx-chevron-down icon-show"></i><i class="bx bx-chevron-up icon-close"></i></a>
                  <div id="accordion-list-1" class="collapse show" data-parent=".accordion-list">
                    <p>
                      usahakan diam dirumah ketika kita sakit, kecuali jika kita ingin pergi ke dokter
                    </p>
                  </div>
                </li>

                <li>
                  <a data-toggle="collapse" href="#accordion-list-2" class="collapsed"><span>02</span> Gunakan masker jika kamu sedang sakit <i class="bx bx-chevron-down icon-show"></i><i class="bx bx-chevron-up icon-close"></i></a>
                  <div id="accordion-list-2" class="collapse" data-parent=".accordion-list">
                    <p>
                      gunakan masker jika kamu berada di kerumunan orang
                    </p>
                  </div>
                </li>

                <li>
                  <a data-toggle="collapse" href="#accordion-list-3" class="collapsed"><span>03</span> Tutuplah mulut dan hidungmu <i class="bx bx-chevron-down icon-show"></i><i class="bx bx-chevron-up icon-close"></i></a>
                  <div id="accordion-list-3" class="collapse" data-parent=".accordion-list">
                    <p>
                      Tutup mulut dan hidungmu ketika batuk dan bersin
                    </p>
                  </div>
                </li>
              </ul>
            </div>
          </div>
          <div class="col-lg-5 align-items-stretch order-1 order-lg-2 img" style='background-image: url("assets/img/save.png");' data-aos="zoom-in" data-aos-delay="150">&nbsp;</div>
        </div>
      </div>
    </section><!-- End Edukasi Section -->

    <!-- ======= donasi Section ======= -->
    <section id="donasi" class="cta">
      <div class="container" data-aos="zoom-in">

        <div class="row">
          <div class="col-lg-9 text-center text-lg-left">
            <h3>Mari Berdonasi</h3>
            <p>Mari bantu saudara kita yang terjangkit virus corona dengan berdonasi. seluruh hasil donasi akan disumbangkan ke pihak yang membutuhkan</p>
          </div>
          <div class="col-lg-3 cta-btn-container text-center">
            <a class="cta-btn align-middle" href="donasi.php">Donasi Sekarang</a>
          </div>
        </div>

      </div>
    </section><!-- End donasi Section -->

    <!-- ======= Frequently Asked Questions Section ======= -->
    <section id="faq" class="faq section-bg">
      <div class="container" data-aos="fade-up">

        <div class="section-title">
          <h2>Frequently Asked Questions</h2>
        </div>

        <div class="faq-list">
          <ul>
            <li data-aos="fade-up" data-aos-delay="100">
              <i class="bx bx-help-circle icon-help"></i> <a data-toggle="collapse" class="collapse" href="#faq-list-1">Apakah virus corona itu? <i class="bx bx-chevron-down icon-show"></i><i class="bx bx-chevron-up icon-close"></i></a>
              <div id="faq-list-1" class="collapse show" data-parent=".faq-list">
                <p style="text-align: justify;">
                  Virus corona (coronavirus) merupakan keluarga besar virus yang ditemukan pada hewan dan manusia.
                  Beberapa varian virus menginfeksi dan diketahui menyebabkan penyakit mulai dari selesma biasa hingga penyakit yang lebih parah seperti Middle East Respiratory Syndrome (MERS) dan Severe Acute Respiratory Syndrome (SARS).
                </p>
              </div>
            </li>

            <li data-aos="fade-up" data-aos-delay="200">
              <i class="bx bx-help-circle icon-help"></i> <a data-toggle="collapse" href="#faq-list-2" class="collapsed">Bagaimana virus corona menyebar? <i class="bx bx-chevron-down icon-show"></i><i class="bx bx-chevron-up icon-close"></i></a>
              <div id="faq-list-2" class="collapse" data-parent=".faq-list">
                <p style="text-align: justify;">
                  Virus corona baru merupakan virus pernapasan yang menyebar terutama melalui kontak dengan orang yang terinfeksi melalui tetesan pernapasan yang dihasilkan ketika seseorang, misalnya, batuk atau bersin, atau melalui tetesan air liur atau keluarnya dari hidung.
                  Penting bahwa setiap orang mempraktikkan kebersihan pernapasan yang baik.
                  Misalnya, bersin atau batuk ke dalam siku yang tertekuk atau gunakan tisu dan buang segera ke tempat sampah yang tertutup.
                  Sangat penting juga bagi orang untuk mencuci tangan secara teratur dengan sabun atau air berbasis alkohol.
                </p>
              </div>
            </li>

            <li data-aos="fade-up" data-aos-delay="300">
              <i class="bx bx-help-circle icon-help"></i> <a data-toggle="collapse" href="#faq-list-3" class="collapsed">Dapatkah terkena virus corona dari hewan peliharaan? <i class="bx bx-chevron-down icon-show"></i><i class="bx bx-chevron-up icon-close"></i></a>
              <div id="faq-list-3" class="collapse" data-parent=".faq-list">
                <p style="text-align: justify;">
                  Tidak. Hingga saat ini tidak ada bukti bahwa hewan peliharaan seperti kucing dan anjing telah terinfeksi atau telah menyebarkan virus 2019-nCoV
                </p>
              </div>
            </li>

            <li data-aos="fade-up" data-aos-delay="400">
              <i class="bx bx-help-circle icon-help"></i> <a data-toggle="collapse" href="#faq-list-4" class="collapsed">Apakah virus ini bisa ditemukan pada orang yang tidak menunjukkan gejala? <i class="bx bx-chevron-down icon-show"></i><i class="bx bx-chevron-up icon-close"></i></a>
              <div id="faq-list-4" class="collapse" data-parent=".faq-list">
                <p style="text-align: justify;">
                  Informasi medis terperinci dari orang diperlukan untuk menentukan periode infeksi 2019-nCoV.
                  Menurut laporan terbaru, mungkin saja orang dapat menularkan virus corona varian baru ini sebelum menunjukkan gejala dengan karakteristik yang signifikan.
                  Namun, berdasarkan data yang tersedia saat ini, orang-orang dengan virus varian baru corona baru pengaruh besar terhadap penyebaran virus.
                </p>
              </div>
            </li>

            <li data-aos="fade-up" data-aos-delay="500">
              <i class="bx bx-help-circle icon-help"></i> <a data-toggle="collapse" href="#faq-list-5" class="collapsed">Apakah aman menerima barang darI China atau daerah lain yang terkonfirmasi? <i class="bx bx-chevron-down icon-show"></i><i class="bx bx-chevron-up icon-close"></i></a>
              <div id="faq-list-5" class="collapse" data-parent=".faq-list">
                <p style="text-align: justify;">
                  Aman.
                  Orang yang menerima paket tidak berisiko tertular virus corona baru.
                  Dari pengalaman dengan virus corona lain, kita tahu bahwa jenis virus ini tidak bertahan lama pada objek, seperti surat atau paket.
                </p>
              </div>
            </li>

          </ul>
        </div>

      </div>
    </section><!-- End Frequently Asked Questions Section -->

    <!-- ======= Contact Section ======= -->
    <section id="contact" class="contact">
      <div class="container" data-aos="fade-up">

        <div class="section-title">
          <h2>Contact</h2>
        </div>

        <div class="row">

          <div class="col-lg-5 d-flex align-items-stretch">
            <div class="info">

              <div class="row mb-4">
                <div class="col-md-4">
                  <img src="assets/img/doni.png" alt="doni" width="100px;">
                </div>
                <div class="col-md-8">
                  <h2 class="pt-2">Doni Anggara</h2>
                  <h6>Teknik Informatika</h6>
                </div>
              </div>

              <div class="address">
                <i class="icofont-google-map"></i>
                <h4>Location:</h4>
                <p>Sukadana Kotakulon, Garut</p>
              </div>

              <div class="email">
                <i class="icofont-envelope"></i>
                <h4>Email:</h4>
                <p>doni@gmail.com</p>
              </div>

              <div class="phone">
                <i class="icofont-phone"></i>
                <h4>WhatsApp:</h4>
                <p>082217459587</p>
              </div>

              <div class="phone">
                <i class="icofont-instagram"></i>
                <h4>Instagram:</h4>
                <p>@doon.i</p>
              </div>
              <div class="phone">
                <i class="icofont-facebook"></i>
                <h4>Facebook:</h4>
                <p>Doni Anggara</p>
              </div>

            </div>

          </div>

          <div class="col-lg-7 mt-5 mt-lg-0 d-flex align-items-stretch">
            <div class="info">
            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3958.1311948465745!2d107.90440401423734!3d-7.225873294783617!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e68b04b35346665%3A0x5ae9da1ae176b850!2sJl.%20Ciledug%2C%20Kota%20Kulon%2C%20Kec.%20Garut%20Kota%2C%20Kabupaten%20Garut%2C%20Jawa%20Barat!5e0!3m2!1sid!2sid!4v1591775458059!5m2!1sid!2sid" width="100%" height="550" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
          </div>
          </div>

        </div>

      </div>
    </section><!-- End Contact Section -->

  </main><!-- End #main -->

  <!-- ======= Footer ======= -->
  <footer id="footer">

    <!-- ======= Cliens Section ======= -->
    <section id="cliens" class="cliens section-bg">
      <div class="container">

        <div class="row" data-aos="zoom-in">

          <div class="col-lg-3 col-md-4 col-6 d-flex align-items-center justify-content-center">
            <img src="assets/img/support/doni.png" class="img-fluid" alt="">
          </div>

          <div class="col-lg-3 col-md-4 col-6 d-flex align-items-center justify-content-center">
            <img src="assets/img/support/doni.png" class="img-fluid" alt="">
          </div>

          <div class="col-lg-3 col-md-4 col-6 d-flex align-items-center justify-content-center">
            <img src="assets/img/support/doni.png" class="img-fluid" alt="">
          </div>

          <div class="col-lg-3 col-md-4 col-6 d-flex align-items-center justify-content-center">
            <img src="assets/img/support/doni.png" class="img-fluid" alt="">
          </div>

        </div>

      </div>
    </section><!-- End Cliens Section -->

    <div class="container footer-bottom clearfix">
      <div class="copyright">
        &copy; Copyright <strong><span>PIDONA</span></strong>. All Rights Reserved
      </div>
      <div class="credits">
        Develop by Doni Anggara
      </div>
    </div>
  </footer><!-- End Footer -->

  <a href="#" class="back-to-top"><i class="ri-arrow-up-line"></i></a>
  <div id="preloader"></div>

  <!-- Vendor JS Files -->
  <script src="assets/vendor/jquery/jquery.min.js"></script>
  <script src="assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="assets/vendor/jquery.easing/jquery.easing.min.js"></script>
  <script src="assets/vendor/php-email-form/validate.js"></script>
  <script src="assets/vendor/waypoints/jquery.waypoints.min.js"></script>
  <script src="assets/vendor/isotope-layout/isotope.pkgd.min.js"></script>
  <script src="assets/vendor/venobox/venobox.min.js"></script>
  <script src="assets/vendor/owl.carousel/owl.carousel.min.js"></script>
  <script src="assets/vendor/aos/aos.js"></script>

  <!-- Template Main JS File -->
  <script src="assets/js/main.js"></script>

</body>

</html>