-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 10, 2020 at 03:56 PM
-- Server version: 10.1.39-MariaDB
-- PHP Version: 7.3.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `doni`
--

-- --------------------------------------------------------

--
-- Table structure for table `berita`
--

CREATE TABLE `berita` (
  `id` int(11) NOT NULL,
  `judul` varchar(200) NOT NULL,
  `deskripsi` text NOT NULL,
  `nama_file` varchar(250) NOT NULL,
  `tanggal` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `berita`
--

INSERT INTO `berita` (`id`, `judul`, `deskripsi`, `nama_file`, `tanggal`) VALUES
(10, 'serangan corona part 1', '<p><strong>Virus Corona atau&nbsp;<em>severe acute respiratory syndrome</em>&nbsp;<em>coronavirus 2</em>&nbsp;(SARS-CoV-2)&nbsp;adalah virus yang menyerang sistem pernapasan.</strong></p>\r\n\r\n<p><strong>&nbsp;</strong><strong>Penyakit karena infeksi virus ini disebut COVID-19.&nbsp;</strong><strong>Virus&nbsp;</strong><strong>Corona&nbsp;</strong><strong>bisa menyebabkan&nbsp;</strong><strong>gangguan ringan pada&nbsp;</strong><strong>sistem pernapasan, infeksi paru-paru yang berat,&nbsp;hingga kematian.</strong></p>\r\n\r\n<p><em>Severe acute respiratory syndrome coronavirus 2</em>&nbsp;(SARS-CoV-2) yang lebih dikenal dengan nama virus Corona adalah jenis baru dari coronavirus yang menular ke manusia. Walaupun lebih banyak menyerang&nbsp;<a href=\"https://www.alodokter.com/alasan-mengapa-lansia-lebih-rentan-terhadap-virus-corona\">lansia</a>, virus ini sebenarnya bisa menyerang siapa saja, mulai dari&nbsp;<a href=\"https://www.alodokter.com/lindungi-bayi-anda-dari-virus-corona\" target=\"_blank\">bayi</a>,&nbsp;<a href=\"https://www.alodokter.com/waspadai-infeksi-virus-corona-pada-anak\" target=\"_blank\">anak-anak</a>, hingga orang dewasa, termasuk ibu hamil&nbsp;dan&nbsp;<a href=\"https://www.alodokter.com/infeksi-virus-corona-pada-ibu-menyusui-ini-yang-perlu-anda-ketahui\" target=\"_blank\">ibu menyusui</a>.</p>\r\n', 'c32a1712-7443-4ae2-8b96-19e14c061576_169.jpeg', '2020-06-10 16:56:16'),
(12, 'Sistus Web gagah SIMONA', '<p>alussssssssssssssssssssssssssssssssssssssss pisaaaaaaaaaaaaaaaaaaaaaan aminn</p>\r\n', 'Screenshot (526).png', '2020-06-10 17:21:29');

-- --------------------------------------------------------

--
-- Table structure for table `donasi`
--

CREATE TABLE `donasi` (
  `id` int(11) NOT NULL,
  `nama` varchar(200) NOT NULL,
  `nohp` varchar(20) NOT NULL,
  `bukti` varchar(200) NOT NULL,
  `jumlah` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `donasi`
--

INSERT INTO `donasi` (`id`, `nama`, `nohp`, `bukti`, `jumlah`) VALUES
(71, 'Doni Anggara', '08123456789', 'banner.jpeg', 10000000);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `nama_depan` varchar(100) NOT NULL,
  `nama_belakang` varchar(100) NOT NULL,
  `username` varchar(200) NOT NULL,
  `password` varchar(200) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `nama_depan`, `nama_belakang`, `username`, `password`) VALUES
(1, 'Doni Anggara', 'Kasep', 'admin', '827ccb0eea8a706c4c34a16891f84e7b');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `berita`
--
ALTER TABLE `berita`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `donasi`
--
ALTER TABLE `donasi`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `berita`
--
ALTER TABLE `berita`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `donasi`
--
ALTER TABLE `donasi`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=73;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
