<?php 
  error_reporting(E_ALL^(E_NOTICE|E_WARNING));
?>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>Data Corona - PIDONA</title>
  <meta content="" name="descriptison">
  <meta content="" name="keywords">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Jost:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">

  <!-- Vendor CSS Files -->
  <link href="assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="assets/vendor/icofont/icofont.min.css" rel="stylesheet">
  <link href="assets/vendor/boxicons/css/boxicons.min.css" rel="stylesheet">
  <link href="assets/vendor/remixicon/remixicon.css" rel="stylesheet">
  <link href="assets/vendor/venobox/venobox.css" rel="stylesheet">
  <link href="assets/vendor/owl.carousel/assets/owl.carousel.min.css" rel="stylesheet">
  <link href="assets/vendor/aos/aos.css" rel="stylesheet">

  <!-- Template Main CSS File -->
  <link href="assets/css/style.css" rel="stylesheet">
</head>

<body>

  <!-- ======= Header ======= -->
  <header id="header" class="fixed-top header-inner-pages">
    <div class="container d-flex align-items-center">
      <h1 class="logo mr-auto"><a href="index.php">PIDONA</a></h1>
      <!-- Bisi ek make logo -->
      <!-- <a href="index.php" class="logo mr-auto"><img src="assets/img/support/doni.png" alt="" class="img-fluid"></a>-->
      <nav class="nav-menu d-none d-lg-block">
        <ul>
          <li class="active"><a href="index.php">Home</a></li>
          <li><a href="berita.php">Berita</a></li>
          <li><a href="donasi.php">Donasi</a></li>
        </ul>
      </nav><!-- .nav-menu -->
    </div>
  </header><!-- End Header -->

  <main id="main">

    <!-- ======= Breadcrumbs ======= -->
    <section id="breadcrumbs" class="breadcrumbs">
      <div class="container">
        <ol>
          <li><a href="index.php">Home</a></li>
          <li>Data Corona</li>
        </ol>
        <h2>Data Corona</h2>
      </div>
    </section><!-- End Breadcrumbs -->

    <!-- ======= Data Section ======= -->
    <section id="portfolio-details" class="portfolio-details">
      <div class="container">
        <div class="portfolio-description">
          <div class="section-title">
            <p>Lihat persebaran virus corona di Indonesia !</p>
          </div>
          <?php  
            $dataa = file_get_contents('https://api.kawalcorona.com/indonesia/');
            $indonesia = json_decode($dataa, true);
          ?>
          <div class="row text-center">
            <?php    
              foreach ($indonesia as $idn) :
            ?>
              <div class="col-md-4 mt-2">
                <div class="card"
                  style="border-top: 2px solid #37517e;
                  transition: all ease-in-out 0.3s;
                  box-shadow: 0px 0 25px 0 rgba(0, 0, 0, 0.1);">
                  <h6 class="text-center mt-3">DATA POSITIF</h6>
                  <h3 style="color: #37517e;" class="text-center">
                    <?= $idn['positif']; ?>
                  </h3>
                </div>
              </div>

              <div class="col-md-4 mt-2">
                <div class="card"
                  style="border-top: 2px solid #37517e;
                  transition: all ease-in-out 0.3s;
                  box-shadow: 0px 0 25px 0 rgba(0, 0, 0, 0.1);">
                  <h6 class="text-center mt-3">DATA SEMBUH</h6>
                  <h3 style="color: #37517e;" class="text-center">
                    <?= $idn['sembuh']; ?>
                  </h3>
                </div>
              </div>

              <div class="col-md-4 mt-2">
                <div class="card"
                  style="border-top: 2px solid #37517e;
                  transition: all ease-in-out 0.3s;
                  box-shadow: 0px 0 25px 0 rgba(0, 0, 0, 0.1);">
                  <h6 class="text-center mt-3">DATA MENINGGAL</h6>
                  <h3 style="color: #37517e;text-align: center;" data-toggle="counter-up">
                    <?= $idn['meninggal']; ?>
                  </h3>
                </div>
              </div>
            <?php    
              endforeach;
            ?>
          </div>

          <div class="row mt-5">
            <div class="col-md-12 mt-3">
              <div class="section-title">
                <p>Lihat persebaran virus corona di Seluruh Dunia !</p>
              </div>
              <div class="card"
                style="border-top: 2px solid #37517e;
                transition: all ease-in-out 0.3s;
                box-shadow: 0px 0 25px 0 rgba(0, 0, 0, 0.1);width: 100%;overflow: scroll;height: 600px;">
                <table class="table table-bordered table-striped sticky mt-0" style="width: 100%;">
                  <?php  
                    $data3 = file_get_contents('https://api.kawalcorona.com/');
                    $negara = json_decode($data3, true);
                  ?>
                  <thead style="background-image: url('assets/img/coro-faq-bg.jpg');text-align: center">
                    <th>No</th>
                    <th>Nama Negara</th>
                    <th>Jumlah Positif</th>
                    <th>Jumlah Meninggal</th>
                  </thead>
                  <tbody>
                    <?php 
                      $a=1;
                      foreach ($negara as $ngr) : 
                    ?>
                    <tr>
                      <td><?= $a++; ?></td>
                      <td><?= $ngr['attributes']['Country_Region']; ?></td>
                      <td><?= $ngr['attributes']['Confirmed']; ?></td>
                      <td><?= $ngr['attributes']['Deaths']; ?></td>
                    </tr>
                  <?php endforeach; ?>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section><!-- End Data Section -->

  </main><!-- End #main -->

  <!-- ======= Footer ======= -->
  <footer id="footer">

    <!-- ======= support Section ======= -->
    <section id="cliens" class="cliens section-bg">
      <div class="container">

        <div class="row" data-aos="zoom-in">

          <div class="col-lg-3 col-md-4 col-6 d-flex align-items-center justify-content-center">
            <img src="assets/img/support/doni.png" class="img-fluid" alt="">
          </div>

          <div class="col-lg-3 col-md-4 col-6 d-flex align-items-center justify-content-center">
            <img src="assets/img/support/doni.png" class="img-fluid" alt="">
          </div>

          <div class="col-lg-3 col-md-4 col-6 d-flex align-items-center justify-content-center">
            <img src="assets/img/support/doni.png" class="img-fluid" alt="">
          </div>

          <div class="col-lg-3 col-md-4 col-6 d-flex align-items-center justify-content-center">
            <img src="assets/img/support/doni.png" class="img-fluid" alt="">
          </div>

        </div>

      </div>
    </section><!-- End support Section -->

    <div class="container footer-bottom clearfix">
      <div class="copyright">
        &copy; Copyright <strong><span>PIDONA</span></strong>. All Rights Reserved
      </div>
      <div class="credits">
        Develop by Doni Anggara
      </div>
    </div>
  </footer><!-- End Footer -->

  <a href="#" class="back-to-top"><i class="ri-arrow-up-line"></i></a>
  <div id="preloader"></div>

  <!-- Vendor JS Files -->
  <script src="assets/vendor/jquery/jquery.min.js"></script>
  <script src="assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="assets/vendor/jquery.easing/jquery.easing.min.js"></script>
  <script src="assets/vendor/php-email-form/validate.js"></script>
  <script src="assets/vendor/waypoints/jquery.waypoints.min.js"></script>
  <script src="assets/vendor/isotope-layout/isotope.pkgd.min.js"></script>
  <script src="assets/vendor/venobox/venobox.min.js"></script>
  <script src="assets/vendor/owl.carousel/owl.carousel.min.js"></script>
  <script src="assets/vendor/aos/aos.js"></script>

  <!-- Template Main JS File -->
  <script src="assets/js/main.js"></script>

</body>

</html>