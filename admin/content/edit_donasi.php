<?php
if(!isset($_SESSION)) {
     session_start();
}
if (isset($_SESSION['username']) and ($_SESSION['password'])):
?>
<script src="https://cdn.ckeditor.com/4.11.2/standard/ckeditor.js"></script>
<?php 
    error_reporting(0);
    $id = $_GET['id'];
    $donasi = $con->query("select * from donasi where id = $id");
    $query = $donasi->fetch_assoc();
?>
<section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Edit Donasi</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form role="form" method='post' enctype="multipart/form-data" action='content/aksi/editdeletedonasi.php?id=<?php echo $id ?>'>
              <div class="box-body">
                <div class="form-group">
                  <label>Jumlah Donasi</label>
                  <input type="text" class="form-control" id="jumlah" name="jumlah" style="border-radius: 25px;" value="<?php echo $donasi['jumlah'] ?>" readonly>
                </div>
                <div class="form-group">
                  <label>Nama Lengkap</label>
                  <input type="text" class="form-control" id="nama" name="nama" style="border-radius: 25px;" value="<?php echo $donasi['nama'] ?>" readonly>
                </div>
                <div class="form-group">
                  <label>No Handphone</label>
                  <input type="text" class="form-control" id="nohp" name="nohp" style="border-radius: 25px;" value="<?php echo $donasi['nohp'] ?>" readonly>
                </div>
                <div class="form-group">
                  <label for="bukti">Bukti Transfer</label>
                  <input type="file" class="form-control-file" name="fupload" id="bukti">
                </div>
              </div>
              <!-- /.box-body -->

              <div class="box-footer">
                <input type="submit" name='posting' class='btn btn-info' value="Posting">
              </div>
            </form>
          </div>
    </div>
    </div>
</section>
<script>
    CKEDITOR.replace( 'deskripsi' );
</script>
<?php 
else:
  echo "<script>;window.location=('index.php');</script>"; 
endif;
?>