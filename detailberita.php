<?php 
  include"config/config.php";
?>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>Detail Berita - PIDONA</title>
  <meta content="" name="descriptison">
  <meta content="" name="keywords">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Jost:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">

  <!-- Vendor CSS Files -->
  <link href="assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="assets/vendor/icofont/icofont.min.css" rel="stylesheet">
  <link href="assets/vendor/boxicons/css/boxicons.min.css" rel="stylesheet">
  <link href="assets/vendor/remixicon/remixicon.css" rel="stylesheet">
  <link href="assets/vendor/venobox/venobox.css" rel="stylesheet">
  <link href="assets/vendor/owl.carousel/assets/owl.carousel.min.css" rel="stylesheet">
  <link href="assets/vendor/aos/aos.css" rel="stylesheet">

  <!-- Template Main CSS File -->
  <link href="assets/css/style.css" rel="stylesheet">
</head>

<body>

  <!-- ======= Header ======= -->
  <header id="header" class="fixed-top header-inner-pages">
    <div class="container d-flex align-items-center">

      <h1 class="logo mr-auto"><a href="index.php">PIDONA</a></h1>
      <!-- Bisi ek make logo -->
      <!-- <a href="index.php" class="logo mr-auto"><img src="assets/img/support/doni.png" alt="" class="img-fluid"></a>-->

      <nav class="nav-menu d-none d-lg-block">
        <ul>
          <li class="active"><a href="index.php">Home</a></li>
          <li><a href="berita.php">Daftar Berita</a></li> 
        </ul>
      </nav>

    </div>
  </header><!-- End Header -->

  <main id="main">

    <!-- ======= Breadcrumbs ======= -->
    <section id="breadcrumbs" class="breadcrumbs">
      <div class="container">

        <ol>
          <li><a href="index.php">Home</a></li>
          <li><a href="berita.php">Daftar Berita</a></li>
          <li>Detail Berita</li>
        </ol>
        <h2>Detail Berita</h2>

      </div>
    </section><!-- End Breadcrumbs -->

    <!-- =======  Details Berita ======= -->

    <?php
      $id = intval($_GET['id']);
      $query =  $con->query("select * from berita WHERE id = {$id}");
      $berita = $query->fetch_assoc();
    ?>
    <section id="portfolio-details" class="portfolio-details">
      <div class="container">

        <div class="row">
          <div class="col-md-8">
            <div class="row">
              <div class="col-md-12">
                <h3><?= $berita['judul']; ?></h3>
                <p><i style="font-size: 10px;"><?= date('d F Y', strtotime($berita['tanggal'])); ?></i></p>
              </div>
            </div>
            <br>
            <div class="text-center">
              <img src="assets/img/berita/<?php echo $berita['nama_file'];?>" width='80%' alt="">
            </div>
            <p style="text-align: justify-all;"><?= $berita['deskripsi']; ?></p>
          </div>
          <div class="col-md-4">
              <div class="card" 
                              style="border-top: 2px solid #37517e;
                              -webkit-box-shadow: 0 10px 20px -10px #940107;
                              box-shadow: 0 10px 20px -10px #000;">
                <div class="card-body">
                  <h5 class="card-title">
                    <i class="icon-file-text"></i> Postingan Terbaru
                  </h5>
                  <?php 
                    $query =  $con->query("select * from berita order by id desc LIMIT 7");
                    while($berita = $query->fetch_assoc()):
                  ?>
                  <li class="card-text" style="list-style: none;">
                    <a href="detail.php?id=<?= $berita['id']; ?>"><?= $berita['judul']; ?></a>
                    <i style="background-color:yellow;font-size: 8px;color: #000;"><?= date('d F Y', strtotime($berita['tanggal'])); ?></i>
                  </li>
                  <?php endwhile; ?>
                </div>
              </div> <!-- End Card -->
          </div>
        </div>
      </div>
    </section><!-- End Detail Berita Section -->

  </main><!-- End #main -->

  <!-- ======= Footer ======= -->
  <footer id="footer">

    <!-- ======= support Section ======= -->
    <section id="cliens" class="cliens section-bg">
      <div class="container">

        <div class="row" data-aos="zoom-in">

          <div class="col-lg-3 col-md-4 col-6 d-flex align-items-center justify-content-center">
            <img src="assets/img/support/doni.png" class="img-fluid" alt="">
          </div>

          <div class="col-lg-3 col-md-4 col-6 d-flex align-items-center justify-content-center">
            <img src="assets/img/support/doni.png" class="img-fluid" alt="">
          </div>

          <div class="col-lg-3 col-md-4 col-6 d-flex align-items-center justify-content-center">
            <img src="assets/img/support/doni.png" class="img-fluid" alt="">
          </div>

          <div class="col-lg-3 col-md-4 col-6 d-flex align-items-center justify-content-center">
            <img src="assets/img/support/doni.png" class="img-fluid" alt="">
          </div>

        </div>

      </div>
    </section><!-- End support Section -->

    <div class="container footer-bottom clearfix">
      <div class="copyright">
        &copy; Copyright <strong><span>PIDONA</span></strong>. All Rights Reserved
      </div>
      <div class="credits">
        Develop by Doni Anggara
      </div>
    </div>
  </footer><!-- End Footer -->

  <a href="#" class="back-to-top"><i class="ri-arrow-up-line"></i></a>
  <div id="preloader"></div>

  <!-- Vendor JS Files -->
  <script src="assets/vendor/jquery/jquery.min.js"></script>
  <script src="assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="assets/vendor/jquery.easing/jquery.easing.min.js"></script>
  <script src="assets/vendor/php-email-form/validate.js"></script>
  <script src="assets/vendor/waypoints/jquery.waypoints.min.js"></script>
  <script src="assets/vendor/isotope-layout/isotope.pkgd.min.js"></script>
  <script src="assets/vendor/venobox/venobox.min.js"></script>
  <script src="assets/vendor/owl.carousel/owl.carousel.min.js"></script>
  <script src="assets/vendor/aos/aos.js"></script>

  <!-- Template Main JS File -->
  <script src="assets/js/main.js"></script>

</body>

</html>